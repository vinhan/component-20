<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">brand</div>

<div class="c-brand">
   <div class="l-inner">
      <div class="c-title">
         <h3 class="c-title__head">安心、安全  鹿追ブランド</h3>
         <span class="c-title__sub">Shikaoi Brand</span>
      </div>

      <ul class="c-brand__lists">
         <li class="c-brand__item">
            <a href="#">
               <figure class="c-brand__img"><img src="/assets/img/home/shikaoi_brand01.jpg" alt="肥沃な大地で生産する安全な農作物"></figure>
               <div class="c-brand__content">
                  <h4 class="c-brand__title">肥沃な大地で生産する安全な農作物</h4>
                  <p class="c-brand__desc">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
               </div>
            </a>
         </li>
         <li class="c-brand__item">
            <a href="#">
               <figure class="c-brand__img"><img src="/assets/img/home/shikaoi_brand02.jpg" alt="北海道を代表する高品質の牛乳"></figure>
               <div class="c-brand__content">
                  <h4 class="c-brand__title">北海道を代表する高品質の牛乳</h4>
                  <p class="c-brand__desc">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
               </div>
            </a>
         </li>
         <li class="c-brand__item">
            <a href="#">
               <figure class="c-brand__img"><img src="/assets/img/home/shikaoi_brand03.jpg" alt="信頼の「鹿追ブランド」牛肉・豚肉"></figure>
               <div class="c-brand__content">
                  <h4 class="c-brand__title">信頼の「鹿追ブランド」牛肉・豚肉</h4>
                  <p class="c-brand__desc">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
               </div>
            </a>
         </li>
      </ul>
   </div>
</div>

<div class="c-dev-title1">gallery</div>

<div class="c-gallery">
   <div class="l-inner-2">
      <div class="c-title">
         <h3 class="c-title__head">フォトギャラリー</h3>
         <span class="c-title__sub">Photo Gallery</span>
      </div>

      <ul class="c-gallery__lists">
         <li class="c-gallery__item">
            <a href="#">
               <div class="c-gallery__frame">
                  <p class="c-gallery__txt01">畑 作</p>
                  <p class="c-gallery__txt02"><img src="/assets/img/home/txt_banner01.png" alt="FARM PRODUCTS"></p>
               </div>
            </a>
         </li>
         <li class="c-gallery__item">
            <a href="#">
               <div class="c-gallery__frame">
                  <p class="c-gallery__txt01">酪 農</p>
                  <p class="c-gallery__txt02"><img src="/assets/img/home/txt_banner02.png" alt="DAIRY FARMING"></p>
               </div>
            </a>
         </li>
         <li class="c-gallery__item">
            <a href="#">
               <div class="c-gallery__frame">
                  <p class="c-gallery__txt01">畜 産</p>
                  <p class="c-gallery__txt02"><img src="/assets/img/home/txt_banner03.png" alt="STOCK RAISING"></p>
               </div>
            </a>
         </li>
      </ul>
   </div>
</div>

<div class="c-dev-title1">information</div>

<div class="c-information">
   <div class="l-inner">
      <div class="c-title">
         <h3 class="c-title__head">インフォメーション</h3>
         <span class="c-title__sub">Information</span>
      </div>

      <ul class="c-information__lists">
         <li class="c-information__item">
            <a href="#">
               <figure class="c-information__img">
                  <img src="assets/img/home/acoop_banner.jpg" alt="国産野菜統一宣言！ 地産地消をおいしく応援中。">
               </figure>

               <div class="c-information__content">
                  <p class="c-information__subHead">Aコープ鹿追店</p>

                  <h4 class="c-information__title">国産野菜統一宣言！ 地産地消をおいしく応援中。</h4>

                  <div class="c-information__desc">
                     Ａコープ鹿追店は生産者と消費者から信頼される店舗を目指し、<br class="pc-only">
                     安心で安全な食品をご提供いたします。
                  </div>
               </div>
            </a>
         </li>
         <li class="c-information__item">
            <a href="#">
               <figure class="c-information__img">
                  <img src="assets/img/home/furusato_banner.jpg" alt="JA鹿追町のふるさとチョイス。">
               </figure>

               <div class="c-information__content">
                  <p class="c-information__subHead">ふるさと納税返礼品［鹿追町］</p>

                  <h4 class="c-information__title">JA鹿追町のふるさとチョイス。</h4>

                  <div class="c-information__desc">
                     安心、安全な鹿追産の牛肉、豚肉、ハンバーグをご提供いたします。<br class="pc-only">
                     是非ご利用ください。
                  </div>
               </div>
            </a>
         </li>
         <li class="c-information__item">
            <a href="#">
               <figure class="c-information__img">
                  <img src="assets/img/home/recruit_banner.jpg" alt="酪農スタッフ募集のお知らせ。">
               </figure>

               <div class="c-information__content">
                  <p class="c-information__subHead">農業求人情報 <span class="c-information__label">NEW</span></p>

                  <h4 class="c-information__title">酪農スタッフ募集のお知らせ。</h4>

                  <div class="c-information__desc">
                     酪農スタッフを募集しています。<br class="pc-only">
                     広大な十勝平野で私たちと一緒に働きませんか！
                  </div>
               </div>
            </a>
         </li>
         <li class="c-information__item">
            <a href="#">
               <figure class="c-information__img">
                  <img src="assets/img/home/recipe_banner.jpg" alt="野菜の大根おろしあえ vol.89">
               </figure>

               <div class="c-information__content">
                  <p class="c-information__subHead">とっておきのレシピ <span class="c-information__label">NEW</span></p>

                  <h4 class="c-information__title">野菜の大根おろしあえ vol.89</h4>

                  <div class="c-information__desc">
                     旬の野菜を大根おろしであえ特性タレをかけました。<br class="pc-only">
                     ごはんのおかずにもビールのおつまににも良く合います。
                  </div>
               </div>
            </a>
         </li>
         <li class="c-information__item">
            <a href="#">
               <figure class="c-information__img">
                  <img src="assets/img/home/jabook_banner.jpg" alt="JA通信11月号を公開しました。">
               </figure>

               <div class="c-information__content">
                  <p class="c-information__subHead">JA通信しかおい</p>

                  <h4 class="c-information__title">JA通信11月号を公開しました。</h4>

                  <div class="c-information__desc">
                     農業ニュースや各種イベントなど楽しい話題をお届けいたします。<br class="pc-only">
                     鹿追町内の皆さまへ毎月無料配布をおこなっています。
                  </div>
               </div>
            </a>
         </li>
      </ul>
   </div>
</div>

