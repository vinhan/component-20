<?php /*========================================
slide
================================================*/ ?>
<div class="c-dev-title1">slide</div>

<div class="c-slide">
   <div class="l-inner-2">
      <ul class="c-slide__slider">
         <li class="c-slide__item">
            <a href="#">
               <figure class="c-slide__img"><img src="/assets/img/home/slider01.jpg" alt="キャッチコピー。キャッチコピー。 キャッチコピー。キャッチコピー。"></figure>

               <div class="c-slide__txt">
                  <p>キャッチコピー。キャッチコピー。</p>
                  <p>キャッチコピー。キャッチコピー。</p>
               </div>
            </a>
         </li>
         <li class="c-slide__item">
            <a href="#">
               <figure class="c-slide__img"><img src="/assets/img/home/slider01.jpg" alt="キャッチコピー。キャッチコピー。 キャッチコピー。キャッチコピー。"></figure>

               <div class="c-slide__txt">
                  <p>キャッチコピー。キャッチコピー。</p>
                  <p>キャッチコピー。キャッチコピー。</p>
               </div>
            </a>
         </li>
         <li class="c-slide__item">
            <a href="#">
               <figure class="c-slide__img"><img src="/assets/img/home/slider01.jpg" alt="キャッチコピー。キャッチコピー。 キャッチコピー。キャッチコピー。"></figure>

               <div class="c-slide__txt">
                  <p>キャッチコピー。キャッチコピー。</p>
                  <p>キャッチコピー。キャッチコピー。</p>
               </div>
            </a>
         </li>
         <li class="c-slide__item">
            <a href="#">
               <figure class="c-slide__img"><img src="/assets/img/home/slider01.jpg" alt="キャッチコピー。キャッチコピー。 キャッチコピー。キャッチコピー。"></figure>

               <div class="c-slide__txt">
                  <p>キャッチコピー。キャッチコピー。</p>
                  <p>キャッチコピー。キャッチコピー。</p>
               </div>
            </a>
         </li>
         <li class="c-slide__item">
            <a href="#">
               <figure class="c-slide__img"><img src="/assets/img/home/slider01.jpg" alt="キャッチコピー。キャッチコピー。 キャッチコピー。キャッチコピー。"></figure>

               <div class="c-slide__txt">
                  <p>キャッチコピー。キャッチコピー。</p>
                  <p>キャッチコピー。キャッチコピー。</p>
               </div>
            </a>
         </li>
      </ul>
   </div>
</div>

<script type="text/javascript">
   $(document).ready(function() {
      $('.c-slide__slider').slick({
         arrows: false,
         dots: true,
      });
   });
</script>
